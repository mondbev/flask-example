Simple Flask app with Nginx & uwsgi

python3.7

- Usage: 
	- add 127.0.0.1 flask-app.com to /etc/hosts
	- clone git
	- cd flask-example
	- python3 -m venv app
	- . app/bin/activate
	- pip3 install wheel uwsgi flask
	- sudo apt install nginx -y
	- sudo chgrp -R www-data /etc/nginx/*
	- sudo chmod -R g+s  /etc/nginx/*
	- sudo systemctl enable --now nginx
	- sudo systemctl status nginx
	- sudo mv inc/app.service /etc/systemd/system/
	- sudo systemctl enable --now app
	- sudo systemctl status app
	- rm /etc/nginx/sites-available/default
	- mv inc/app /etc/nginx/sites-available/
	- ln -s /etc/nginx/sites-available/app /etc/nginx/sites-enabled
	- browse to flask-app.com:5000
