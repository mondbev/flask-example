from flask import Flask
from flask import render_template


app = Flask(__name__)

@app.route('/')
def index():
    # HTML --> HyperText Markup Language
    return render_template("index.html" )

@app.route("/bimba")
def bimba():
    return "Alex wasn't here"

app.run()
